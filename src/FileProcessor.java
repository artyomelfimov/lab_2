import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public abstract class FileProcessor {
    protected List<String> fileNames = new ArrayList<>();

    public void start() {
        getFiles();
        processFiles();
    }

    protected abstract void processFiles();

    private void getFiles() {
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("Enter file name(to stop write exit): ");
            String fileName = sc.nextLine();
            if ("exit".equals(fileName.toLowerCase(Locale.ROOT))) {
                break;
            }
            fileNames.add(fileName);
        }

        sc.close();
    }
}
