import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class WordFileProcessor extends FileProcessor{
    @Override
    public void processFiles() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileNames.get(0)));
            List<String> wordsToFind = reader.lines()
                    .map(word -> word.toLowerCase(Locale.ROOT))
                    .collect(Collectors.toList());
            reader.close();

            fileNames.remove(0);
            for (String fileName : fileNames) {
                boolean foundAll = true;
                reader = new BufferedReader(new FileReader(fileName));
                String lines = reader.lines()
                        .collect(Collectors.joining());

                for (String word : wordsToFind) {
                    if (!lines.contains(word)) {
                        foundAll = false;
                        break;
                    }
                }

                if (foundAll) {
                    System.out.println("All words found in file: " + fileName);
                }
                reader.close();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
