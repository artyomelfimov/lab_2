import java.io.*;
import java.util.Locale;
import java.util.stream.Collectors;

public class ConvertLiteralsFileProcessor extends FileProcessor{
    @Override
    public void processFiles() {
        try {
            for (String fileName : fileNames) {
                BufferedReader reader = new BufferedReader(new FileReader(fileName));
                String fileDataInUpperCase = reader.lines()
                        .map(line -> line.toUpperCase(Locale.ROOT))
                        .collect(Collectors.joining("\n"));

                reader.close();

                PrintWriter fileWriter = new PrintWriter(new FileWriter(fileName));
                fileWriter.println(fileDataInUpperCase);
                fileWriter.close();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
